import { Card } from 'native-base'
import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, ScrollView} from 'react-native'
import { Image, Icon } from 'react-native-elements'

export class Unlimited extends Component {
    render() {
         const data = this.props?.route?.params?.data ? this.props.route.params.data : ''
        return (            
            <View style={styles.container}>  
                <ScrollView>      
                    <View>
                        {/* {this.props.route.params.name} */}
                        <Text style={styles.text_title} > {data && data.title} </Text>
                        <Text > {data && data.rating} </Text>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 5 }}>
                        <Image
                            style={{ width: Dimensions.get('screen').width, height: 200 }}
                            resizeMode='cover'
                            source={{ uri: data && data.medium_cover_image}}
                        />
                        <Text style={{ margin: 5, color: 'black', fontWeight: 'bold' }}>{data && data.summary}</Text>
                    </View>
                </ScrollView>       
                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: -10 }}>
                    <Card style={{ width: Dimensions.get('screen').width, height: 50, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <Icon
                            name='sc-facebook'
                            type='evilicon'
                            color='#517fa4'
                            size={50}
                        />
                        <Icon
                            name='sc-twitter'
                            type='evilicon'
                            color='#42a5f5'
                            size={50}
                        />
                    </Card>
                </View>
            </View>
        )
    }
}

export default Unlimited
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //margin: 10,
    },
    text_title: {
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 10,
    }
})