import { Card } from 'native-base';
import React, { Component } from 'react'
import { StyleSheet, Text, View, FlatList, Dimensions, TouchableOpacity } from 'react-native'
import { Divider } from 'react-native-elements';
import { round } from 'react-native-reanimated';
export const apikey = '5daca7997f064500958f12acbcf1b889';

export class Drafts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            schemes: [],
        }
    }
    fetchData = async () => {
        fetch(`https://yts.mx/api/v2/list_movies.json?limit=15`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'content-type': 'application/json',
            }
        })
            .then((res) => res.json())
            .then((result) => {
                this.setState({
                    schemes: result.data.movies,
                })
            })
            .catch((err) => console.log(err))
    }
    componentDidMount() {
        this.fetchData()
    }
    render() {
        const { schemes } = this.state;
        return (
            <View style={styles.container}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={schemes}
                    keyExtractor={(item) => item.title}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={
                            () => this.props.navigation.jumpTo('Unlimited',{data:item})
                        }>
                            <Items
                                keyExtractor={(item) => item.id}
                                data={item}
                            />
                    </TouchableOpacity>
                    }
                />
                <View style={{ width: Dimensions.get('screen').width }}>
                    <Card style={{ width: Dimensions.get('screen').width, height: 50 }}>

                    </Card>
                </View>
            </View>
        )
    }
}

export default Drafts

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

const Items = ({ navigate, data }) => {
    return (       
            <View style={{ flexDirection: 'row', margin: 5, width: Dimensions.get('screen').width }}>
                <View style={{ width: Dimensions.get('screen').width - 200, marginRight: 150 }}>
                    <Text style={{ fontWeight: 'bold', marginBottom: 10 }} numberOfLines={2} ellipsizeMode="tail" >{data.title}</Text>
                    <Text>{data.rating}</Text>
                </View>
                <View>
                    <Text style={{ fontWeight: 'bold' }}>More</Text>
                </View>
            </View>
    )
}

