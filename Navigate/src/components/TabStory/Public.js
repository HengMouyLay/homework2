import { Card } from 'native-base'
import React, { Component } from 'react'
import { Dimensions, StyleSheet, Text, View } from 'react-native'

export class Public extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: Dimensions.get('screen').width / 2 - 10, marginRight: 10, }}>
                    <Card style={{ width: Dimensions.get('screen').width / 2 - 10, height: 120 }}>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={{ fontWeight: 'bold', fontSize: 20 }}> React Native: Stack Navigator</Text>
                        <Text style={{ marginTop: 10 }}> 10 Mar 2020</Text>
                        <Text style={{ marginLeft: 150 }}> More</Text>
                    </Card>

                </View>
                <View style={{ width: Dimensions.get('screen').width / 2 - 10 }}>
                    <Card style={{ width: Dimensions.get('screen').width / 2 - 10, height: 120 }}>
                        <Text numberOfLines={2} ellipsizeMode="tail" style={{ fontWeight: 'bold', fontSize: 20 }}> React Native: Stack Navigator</Text>
                        <Text style={{ marginTop: 10 }}> 10 Mar 2020</Text>
                        <Text style={{ marginLeft: 150 }}> More</Text>
                    </Card>
                </View>
            </View>
        )
    }
}

export default Public
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    }
})