
import { Card } from 'native-base';
import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native'
import { Icon } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';

export function HomeDetail({ route }) {
    const { obj, } = route.params;
    return (
        <View style={styles.container}>
            <ScrollView>
                <View>
                    <Text style={styles.text_title}> {obj.title} </Text>
                    <Text > {obj.rating} </Text>
                </View>
                <View style={{ alignItems: 'center', marginTop: 5 }}>
                    <Image
                        style={{ width: Dimensions.get('screen').width, height: 200 }}
                        resizeMode='cover'
                        source={{ uri: obj.medium_cover_image }}
                    />
                    <Text style={{ marginTop: 5, color: 'black', fontWeight: 'bold' }}>{obj.summary}</Text>
                </View>
            </ScrollView>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: -15 }}>
                <Card style={{ width: Dimensions.get('screen').width, height: 50,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                    <Icon
                        name='sc-facebook'
                        type='evilicon'
                        color='#517fa4'
                        size={50}
                    />
                    <Icon
                        name='sc-twitter'
                        type='evilicon'
                        color='#42a5f5'
                        size = {50}
                    />
                </Card>
            </View>
        </View>

    )
}


export default HomeDetail
const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10,
    },
    text_title: {
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 10,
    }
})
