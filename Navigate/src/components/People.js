import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions} from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

export class People extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: [
                { title: 'Bourn Sochhai' ,desc : "Co-founder and CEO of S-PEOPLE"},
                { title: 'Poch Sitha', desc: "Who developer Subscripe to mu email list now" },
                { title: 'Pheng SokHeng', desc: "4-Nov-2020" },
                { title: 'Keo Romechetra', desc: "4-Nov-2020E" },
                { title: 'Facebook Gaming', desc: "4-Nov-2020" },
                { title: 'Keo Romechetra', desc: "4-Nov-2020E" },
                { title: 'Facebook Gaming', desc: "4-Nov-2020" },
            ]
        }
    }
    componentDidMount() {
        let arr = this.state.profile.map((item, index) => {
            item.isSelected = false
            return { ...item }
        })
        this.setState({ profile: arr })
    }
    handleOnButton = (ind) => {
        const { profile } = this.state;
        let arr = profile.map((item, index) => {
            if (ind == index) {
                item.isSelected = !item.isSelected
            }
            return { ...item }
        })
        this.setState({ profile: arr });
    }
    render() {
        const { profile} = this.state;
        return (
            <View style={styles.container}> 
            <ScrollView showsVerticalScrollIndicator={false}>
            {profile.map((item,index) =>
                <View style={{ width: Dimensions.get('screen').width, margin: 5, flexDirection: 'row' ,justifyContent:'flex-end'}} >
                    <View style={{height:100}}>
                        <Text style={styles.left_comp_style, { fontWeight: 'bold', fontSize: 17 }}>{item.title}</Text>
                        <Text style={styles.left_comp_style}>{item.desc}</Text>
                    </View>
                    <View>
                        <TouchableOpacity style={item.isSelected ? styles.following_style : styles.follow_style}
                         onPress={() => this.handleOnButton(index)}>
                            <Text style={item.isSelected ? styles.color_title_btn_following : styles.color_title_btn_follow}>{item.isSelected ? "Following" : "Follow"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}    
                </ScrollView>
            </View>
            
        )
    }
}

export default People

const styles = StyleSheet.create({
    container:{
        flex:1,   
    },
    left_comp_style:{
        width:Dimensions.get('screen').width-250,
        marginRight:5,
        
    },
     right_comp_style: {
        width:100,
        height:40,
        borderColor:'green',
        borderWidth:1,       
        marginRight:10,
        justifyContent:'center',
        backgroundColor:'transparent',
        alignItems:'center',
        borderRadius:10,
        marginTop:10,
    },
    follow_style: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'green',
        width: 100,
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight:10

    },
    following_style: {
        backgroundColor: 'green',
        borderWidth: 1,
        borderColor: 'green',
        width: 110,
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight:10
    },
    color_title_btn_following:
    {
        color: 'white',
    },
    color_title_btn_follow:
    {
        color: 'green',
    }
})