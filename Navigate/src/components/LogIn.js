import React, { Component } from 'react'
import { Text, View, StyleSheet,TouchableOpacity, Dimensions } from 'react-native'
import { Input, Icon } from 'react-native-elements'



export class LogIn extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title_style}> Hey APP </Text>
                <Input
                    placeholder="UserName"
                    leftIcon={{ type: 'material', name: 'account-circle' }}
                    style={styles.input_style}
                    inputContainerStyle={{borderBottomColor:'white'}}
                    inputStyle={{marginLeft:-30}}
                    // onChangeText={value => this.setState({ comment: value })}
                />
                <Input
                    placeholder="PassWord"
                    leftIcon={{ type: 'material', name: 'lock', color:'white' }}
                    style={styles.input_style}
                    inputContainerStyle={{ borderBottomColor: 'white' }}
                    inputStyle={{ marginLeft: -30 }}
                    secureTextEntry={true}
                // onChangeText={value => this.setState({ comment: value })}
                />
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate('Home')}
                >
                    {/* <Icon name= 'lock' type='material' /> */}
                    <Text style={{color:'white'}}>LogIn</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default LogIn
const styles = StyleSheet.create({
    container:{
                flex:1,
                justifyContent:'center',
                alignItems:'center',
    },
    title_style:{
                fontSize:40,
                color:'red',
                fontStyle:'italic',
                marginBottom:20,
    },
    input_style:{
                borderWidth:1,
                borderColor:'black',
                borderRadius:30,
                paddingLeft:30,
                color:'white',
                backgroundColor:'black',
               
    },
    button:{
        backgroundColor:'red',
        width:Dimensions.get('screen').width-20,
        height:50,
        borderRadius:30,
        justifyContent:'center',
        alignItems:'center',
    }

})