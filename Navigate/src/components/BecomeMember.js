import { Button } from 'native-base';
import React, { Component } from 'react'
import { Dimensions, StyleSheet, Text, View } from 'react-native'
import { Icon, PricingCard, } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';

export class BecomeMember extends Component {
    render() {
        return (
            <ScrollView >
                <View style={{backgroundColor:'black'}}><Icon
                    raised
                    name='close'
                    type='font-awesome'
                    color='black'
                    onPress={() => this.props.navigation.goBack()} /></View>
                <View style={styles.container}>
                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 20 }}>Unlimited Reading</Text>
                    <Text style={{ color: 'black', fontSize: 20, marginBottom: 20 }}>Free for 1 Month</Text>
                    <Text style={{ marginLeft: 20 }}>Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime</Text>
                    <PricingCard containerStyle={{ width: Dimensions.get('screen').width }}
                        color="#4f9deb"
                        price="$5/month"
                        info={['First Month Free',]}
                        button={{ title: 'Start your free trial', color: 'black' }}
                        infoStyle={{ color: 'black' }}
                    />
                    <PricingCard containerStyle={{ width: Dimensions.get('screen').width, backgroundColor: 'transparent', }}
                        color="#4f9deb"
                        price="$50/year"
                        info={['First Month Free',]}
                        button={{ title: 'Start your free trial', color: 'transparent' }}
                        infoStyle={{ color: 'black' }}
                    />
                    <Text style={{ marginLeft: 20 }}>
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                        Read on any device ,ad-free,and offline. We'll remind you 3 days before your trail ends. Cancel anytime
                    </Text>
                </View>
            </ScrollView>
        )
    }
}

export default BecomeMember

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 50,
        //justifyContent: 'center',
    }

})