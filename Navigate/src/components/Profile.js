
import React, { Component } from 'react'
import { Text, View, StyleSheet, ImageBackground, Dimensions, Image, TouchableOpacity } from 'react-native'
import { Avatar, Icon } from 'react-native-elements'


export class Profile extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.image}
                    source={require('./../../images/bg_sakura.jpg')}
                />
                <View style={{ marginTop: -75, alignItems: 'center' }}>
                    <Avatar
                        avatarStyle={{ borderWidth: 2, borderColor: 'red', }}
                        rounded
                        size="xlarge"
                        source={{ uri: 'https://lavinephotography.com.au/wp-content/uploads/2017/01/PROFILE-Photography-112.jpg'}}
                    />
                    <Text style={{ marginTop: 5, fontSize: 20, fontWeight: 'bold' }}>Kungfu Panda</Text>
                </View>
                <View style={{ flexDirection: 'row',marginTop:10 }}>
                    <View>
                        <TouchableOpacity style={styles.btn_story_style}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Icon
                                    reverse
                                    name='add'
                                    type='material'
                                    color="gray"
                                    size={10}
                                />
                                <Text style={{ color: 'white' }}>Add Story</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity style={{ width:40,height:30,backgroundColor:'lightgray',borderRadius:5,alignItems:'center',marginLeft:10 }}>
                            <Text style={{color:'black',fontWeight:'bold',fontSize:15}}>...</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent:'center',
        //paddingTop:100,
        //justifyContent:'flex-start',
    },
    image: {
        resizeMode: "cover",
        justifyContent: "center",
        height: 200,
        width: Dimensions.get('screen').width,
    },
    btn_story_style: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'blue',
        width: Dimensions.get('screen').width - 100,
        height: 30,
        borderRadius: 5,

    }
})