import { Item } from 'native-base';
import React, { Component, useState } from 'react'
import { Button, Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
//import { art } from './TopPicData'

export class TopPic extends Component {
    constructor(props) {
        super(props);
        this.state = {
            art : [
                { title: 'Art' },
                { title: 'Book' },
                { title: 'Comics' },
                { title: 'Film' },
                { title: 'Gaming' },
            ]
        }
    }
    componentDidMount(){
        let arr = this.state.art.map((item,index) =>
        {
            item.isSelected = false
            return{...item}
        })
        this.setState({art:arr})
    }
    handleOnButton = (ind) => {
        const {art} = this.state;
        let arr = art.map((item,index)=>{
            if(ind==index)
            {
                item.isSelected =!item.isSelected
            }
            return{...item}
        })
        this.setState({art:arr});
    }

    render() {
        const { art } = this.state;
        return (
            <View style={styles.container}>
                {art.map((item,index) =>
                    <View style={{ width: Dimensions.get('screen').width, margin: 5, flexDirection: 'row' }} >
                        <Text style={styles.text_style_title} onPress={() => this.props.navigation.jumpTo('Publication')}>{item.title}</Text>
                        <TouchableOpacity style={item.isSelected ? styles.following_style : styles.follow_style}
                            onPress={() => this.handleOnButton(index)}
                        >
                            <Text style={item.isSelected ? styles.color_title_btn_following : styles.color_title_btn_follow}>{item.isSelected ? "Following" : "Follow"}</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </View>

        )
    }
}

export default TopPic

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent:'center',
        //alignItems:'center',
    }, text_style_title: {
        fontSize: 20,
        color: 'black',
        width: Dimensions.get('screen').width - 120, marginTop: 10
    },
    follow_style: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: 'green',
        width: 90,
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    following_style: {
        backgroundColor: 'green',
        borderWidth: 1,
        borderColor: 'green',
         width: 100,
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    color_title_btn_following:
    {
        color: 'white',
    },
    color_title_btn_follow:
    {
        color: 'green',
    }

})
// var isFollowing = false;
// export const handleOnButton = () => {
//     //alert(isFollowing)
//     isFollowing = !isFollowing;
//     alert(isFollowing)

// }
// const Items = ({ title }) => {
//     return (
//         <View style={{ width: Dimensions.get('screen').width, margin: 5, flexDirection: 'row' }} >
//             <Text style={styles.text_style, { width: Dimensions.get('screen').width - 120, marginTop: 10 }}>{title}</Text>
//             <TouchableOpacity style={isFollowing ? styles.following_style : styles.follow_style}
//                 onPress={() => handleOnButton()}
//             >
//                 <Text style={{ color: 'green' }}>{isFollowing ? "Following" : "Follow"}</Text>
//             </TouchableOpacity>

//         </View>
//     )
// }
