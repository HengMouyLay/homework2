import { DrawerActions, NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { HomeScreen } from './HomeScreen'
import { HomeDetail } from './HomeDetail'
import LogIn from './LogIn'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import TopPic from './TopPic'
import Profile from './Profile'
import Story from './Story'
import BecomeMember from './BecomeMember'
import People from './People'
import Publication from './Publication'
import Drafts from './../components/TabStory/Drafts'
import Public from './../components/TabStory/Public'
import Unlimited from './../components/TabStory/Unlimited'

import { createDrawerNavigator } from "@react-navigation/drawer";
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Icon } from 'react-native-elements'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const MainStackNavigator = () => {
    //const { name } = route.params
    return (
        <Stack.Navigator>
            <Stack.Screen name="LogIn" component={LogIn} options={{headerShown:false}}/>
            <Stack.Screen name="Home"
                component={DrawerNavigator}
                options={{ 
                    // headerStyle: {
                    //     backgroundColor: 'black',
                    // },
                    headerShown:false,
                    headerTitleStyle: {
                        color: 'black',
                    },
                }}            
            />
            <Stack.Screen name="HomeDetail" component={HomeDetail} options={{ headerShown: true }} />  
        </Stack.Navigator>
    );
};
const RootStackNavigator = () => {
    //const { name } = route.params
    return (
        <Stack.Navigator mode="mo">
            <Stack.Screen name="LogIn" component={MainStackNavigator} options={{ headerShown: false }} />            
            <Stack.Screen name="Member" component={BecomeMember} options={{ headerShown: true }} />
        </Stack.Navigator>
    );
};
function TabInterest() {
    return (
      <Tab.Navigator>
        <Tab.Screen name="TopPic" component={TopPic} />
        <Tab.Screen name="People" component={People} />
        <Tab.Screen name="Publication" component={Publication} />
      </Tab.Navigator>
    );
  }
  function TabStory() {
      return (
        <Tab.Navigator initialRouteName="Drafts">
          <Tab.Screen name="Drafts" component={Drafts} />
          <Tab.Screen name="Public" component={Public} />
          <Tab.Screen name="Unlimited" component={Unlimited} />
        </Tab.Navigator>
      );
    }

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator initialRouteName="Home" >
            <Drawer.Screen name="Profile" component={Profile} options={{headerShown:true}}/>
            <Drawer.Screen name="Home" component={HomeScreen} options={{ headerShown: true }}/>
            <Drawer.Screen name="Interest" component={TabInterest} options={{ headerShown: true }}/>
            <Drawer.Screen name="BecomeMember" component={BecomeMember} options={{ headerShown: false }}/>
            <Drawer.Screen name="Story" component={TabStory} options={{ headerShown: true }}/>
        </Drawer.Navigator>
    );
};

export class NavigationDemo extends Component {
    render() {
        return (
            <NavigationContainer>
                <RootStackNavigator />
                {/* <DrawerNavigator/>  */}
            </NavigationContainer>
        )
    }
}

export default NavigationDemo
