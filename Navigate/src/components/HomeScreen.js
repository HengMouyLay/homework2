import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, Image, Dimensions, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Card } from 'react-native-elements';
// export const baseURL = "http://192.168.201.245/hspis/api/";
// export const apikey = '5daca7997f064500958f12acbcf1b889';


export class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            schemes: [],
            page:1,
            loading:false,
            total_page:1
        }
    }
    fetchData = async () => {
       
        fetch(`https://yts.mx/api/v2/list_movies.json?limit=10&page=${this.state.page}`, {
            method: 'GET',
            headers: {
                'Accept': 'Application/json',
                // 'Content-Type': 'Application/json',
            }
        })
            .then((res) => res.json())
            .then((result) => {
                console.log("result:")
                this.setState({                    
                    schemes:this.state.schemes.concat(result.data.movies),
                    total_page: result.data.movies.movie_count / result.data.movies.limit,
                })
            })
            .catch((err) => console.log(err))
           console.log("result:",result.data.movies)
    }
    componentDidMount() {
        this.fetchData();
    }
    loadMoreData = () => {
    //    if(this.state.page>=this.state.total_page)
    //     {
    //         this.setState({loading:false})
    //     }
    //     else
    //     {
            //alert(this.state.page)
            this.setState({page:this.state.page+1,loading:true}, this.fetchData)
       // }
    }
    footerComponent = () => {
        return(
            this.state.loading ? <ActivityIndicator size="large" /> : null
        )        
    }
    render() {
        const { schemes } = this.state;
        return (
            <View>
                <Text style={{ fontWeight: 'bold', marginLeft: 10, marginTop: 10, fontSize: 20 }}>New Daily Read</Text>
                <FlatList
                    //keyExtractor={item.}
                    data={schemes}
                    keyExtractor={(item) => item.id}
                    onEndReached ={this.loadMoreData}
                    ListFooterComponent={this.footerComponent}
                    renderItem={({ item }) =>
                        <Items
                            data = {item}
                            navigation={this.props.navigation}                                  
                        />}
                />
            </View>
        )
    }
}


export default HomeScreen

function Items({ navigation,data }) {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() =>
            {
                navigation.navigate('HomeDetail', { obj: data})
            }
             }>
                <Card containerStyle={{ width: Dimensions.get('screen').width, }}>
                    <View style={{ flexDirection: 'row', width: Dimensions.get('screen').width / 2, height: 120, marginTop: 5 }}>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={styles.text_title} numberOfLines={2} ellipsizeMode="tail" >{data.title} </Text>
                            <Text style={styles.text_style} numberOfLines={2} ellipsizeMode="tail" >{data.summary}</Text>
                            <Text >{data.rating}</Text>
                        </View>
                        <View style={{ marginLeft: 5, marginRight: 5 }}>
                            <Image style={styles.image} source={{ uri: data.medium_cover_image }} />
                        </View>
                    </View>
                </Card>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    text_title: {
        fontWeight: 'bold',
        margin: 5,
        fontSize: 20,
    },
    text_style: {
        fontSize:15,
    },
    image: {
        width: 150,
        height: 100,
        resizeMode: 'cover',
    }
})